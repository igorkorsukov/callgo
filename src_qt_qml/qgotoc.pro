TEMPLATE = app

QT += qml quick
CONFIG += c++11

LIBS += \
    $$PWD/../gotoc.a

DEPENDPATH += \
    $$PWD/..

PRE_TARGETDEPS += \
    $$PWD/../gotoc.a

win32: {
    LIBS += -lwinmm -lWs2_32
}

RESOURCES += qml.qrc

SOURCES += main.cpp \
    qgotoc.cpp \
    qgotocqml.cpp \
    qgoform.cpp \
    qgolistmodel.cpp \
    rpc.cpp

HEADERS += \
    qgotoc.h \
    qgotocqml.h \
    qgoform.h \
    rpc.h \
    qgolistmodel.h

OTHER_FILES += \
    main.qml

DISTFILES += \
    Login.qml \
    GoToCComp/GoForm.qml \
    Points.qml
