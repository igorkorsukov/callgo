#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTextCodec>

#include "qgotocqml.h"

int main(int argc, char *argv[])
{
    QTextCodec *textCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(textCodec);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QGoToCQml::registerObjects(&engine);
    QGoToCQml::registerTypes("QGoToC");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
