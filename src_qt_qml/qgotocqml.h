#ifndef QGOTOCQML_H
#define QGOTOCQML_H

#include <QObject>
#include <QQmlEngine>

class QGoToCQml : public QObject
{
    Q_OBJECT
public:
    explicit QGoToCQml(QObject *parent = 0);

    static QGoToCQml* instance() {
        static QGoToCQml i;
        return &i;
    }

    static void registerObjects(QQmlEngine* engine);
    static void registerTypes(const char *uri);

    QQmlEngine* engine();

private:

    void init(QQmlEngine *e);

    QQmlEngine* m_engine;
};

#endif // QGOTOCQML_H
