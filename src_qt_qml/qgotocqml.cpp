#include "qgotocqml.h"
#include <QQmlContext>
#include <QtQml>

#include "qgoform.h"
#include "qgolistmodel.h"

QGoToCQml::QGoToCQml(QObject *parent)
    : QObject(parent), m_engine(0)
{

}

void QGoToCQml::registerObjects(QQmlEngine* engine)
{
    engine->rootContext()->setContextProperty("go", QGoToCQml::instance());
    QGoToCQml::instance()->init(engine);
}

void QGoToCQml::registerTypes(const char *uri)
{
    qmlRegisterType<QGoForm>(uri, 1, 0, "QGoForm");
    qmlRegisterType<QGoListModel>(uri, 1, 0, "QGoListModel");
}

void QGoToCQml::init(QQmlEngine *e)
{
    m_engine = e;
}

QQmlEngine* QGoToCQml::engine()
{
    return m_engine;
}
