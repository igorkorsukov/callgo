package service

import (
	"fmt"
	"time"

	"encoding/json"

	"github.com/mailru/easyjson/jwriter"
)

type Item struct {
	ID       uint64
	Name     string
	BirthDay time.Time
	Phone    string
	Siblings int
	Spouse   bool
	Money    float32
}

type Service struct {
}

func (s Service) Items() []Item {

	list := make([]Item, 100000)
	now := time.Now()
	for i := 0; i < 100000; i++ {
		list[i] = Item{
			ID:       uint64(i),
			Name:     fmt.Sprint("Item ", i),
			BirthDay: now,
			Phone:    "+79003517159",
			Money:    42.01,
		}
	}
	return list
}

type Stat struct {
	Strt time.Time
	Prev time.Time
}

func (st *Stat) Start() {
	st.Strt = time.Now()
	st.Prev = st.Strt
}

func (st *Stat) Step() time.Duration {
	now := time.Now()
	e := now.Sub(st.Prev)
	st.Prev = now
	return e
}

func (st *Stat) Total() time.Duration {
	return time.Since(st.Strt)
}

func (s Service) ItemsJSON([]byte) []byte {

	st := Stat{}
	st.Start()
	fmt.Println("go: time: begin ItemsJSON")
	items := s.Items()
	fmt.Println("go: time: end s.Items: ", st.Step())

	b, _ := json.Marshal(items)

	fmt.Println("go: time: end json Marshal: ", st.Step(), ", b: ", len(b))
	return b
}

func (s Service) ItemsEasyJSON([]byte) []byte {

	st := Stat{}
	st.Start()
	fmt.Println("go: time: begin ItemsEasyJSON")
	items := s.Items()
	fmt.Println("go: time: end s.Items: ", st.Step())

	w := jwriter.Writer{}
	w.RawByte('[')
	end := len(items) - 1
	for i, it := range items {

		it.MarshalEasyJSON(&w)
		if i < end {
			w.RawByte(',')
		}
	}
	w.RawByte(']')

	b, _ := w.BuildBytes()

	fmt.Println("go: time: end easyjson Marshal: ", st.Step(), ", b: ", len(b))
	return b
}

func (s Service) ItemsArrRawJSON([]byte) []byte {

	st := Stat{}
	st.Start()
	fmt.Println("go: time: begin ItemsArrRawJSON")
	items := s.Items()
	fmt.Println("go: time: end s.Items: ", st.Step())

	w := jwriter.Writer{NoEscapeHTML: true}
	w.RawByte('[')
	end := len(items) - 1
	for i, it := range items {

		w.RawByte('[')
		w.Uint64(it.ID)
		w.RawByte(',')
		w.String(it.Name)
		w.RawByte(',')
		w.Int64(it.BirthDay.Unix())
		w.RawByte(',')
		w.String(it.Phone)
		w.RawByte(',')
		w.Int(it.Siblings)
		w.RawByte(',')
		if it.Spouse {
			w.Int(1)
		} else {
			w.Int(0)
		}
		w.RawByte(',')
		w.Float32(it.Money)
		w.RawByte(']')

		if i < end {
			w.RawByte(',')
		}
	}
	w.RawByte(']')

	b, _ := w.BuildBytes()

	fmt.Println("go: time: end ArrRaw Marshal: ", st.Step(), ", b: ", len(b))
	return b
}

func (s Service) SaveItemsB(b []byte) []byte {
	fmt.Println("SaveItemsB: ", b)
	return nil
}

//ItemsB b:  2077781
//void Model::loadGo() ba.size: 2077781
//void Model::loadGo() arr.size: 100000
//void Model::loadGo() 0 "Item 0"
//void Model::loadGo() elapsed: 170 ms count: 100000
//void Model::loadC() elapsed: 62 ms count: 100000

//ItemsB o b:  3377781
//void Model::loadGo() ba.size: 3377781
//void Model::loadGo() arr.size: 100000
//void Model::loadGo() 0 "Item 0"
//void Model::loadGo() elapsed: 178 ms count: 100000
//void Model::loadC() elapsed: 63 ms count: 100000

// go get gopkg.in/vmihailenco/msgpack.v2
