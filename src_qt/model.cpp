#include "model.h"

#include <QDebug>
#include <QElapsedTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "qgotoc.h"

#include "service.h"

Model::Model()
{

}

qint64 Model::loadJSON()
{
    return doLoadObj(110);
}

qint64 Model::loadEasyJSON()
{
    return doLoadObj(111);
}

qint64 Model::loadArrRawJSON()
{
    return doLoadArr(112);
}

qint64 Model::doLoadObj(int i)
{
    QGoToC *g = QGoToC::instance();

    QList<Item*> items;

    QElapsedTimer timer;

    qDebug() << "qt: time: obj start" << i;
    timer.start();
    qint64 prev = 0;

    QByteArray ba = g->goCall(i);

    qDebug() << "qt: time: end goCall" << i << ": " << (timer.elapsed() - prev) << "ms" << ", ba.size:" << ba.size();
    prev = timer.elapsed();

    QJsonDocument json = QJsonDocument::fromJson(ba);
    QJsonArray arr = json.array();

    const QString ID("ID");
    const QString Name("Name");
    const QString BirthDay("BirthDay");
    const QString Phone("Phone");
    const QString Siblings("Siblings");
    const QString Spouse("Spouse");
    const QString Money("Money");

    qDebug() << "qt: time: end prepare json: " << (timer.elapsed() - prev) << "ms" << ", arr count:" << arr.size();
    prev = timer.elapsed();
    foreach (const QJsonValue &val, arr) {

        QJsonObject obj = val.toObject();
        Item *i = new Item();
        i->id = obj.value(ID).toInt();
        i->name = obj.value(Name).toString();
        //i->birthDay = QDateTime::fromMSecsSinceEpoch(obj.value(BirthDay).toInt());
        i->phone = obj.value(Phone).toString();
        i->siblings = obj.value(Siblings).toInt();
        i->spouse = obj.value(Spouse).toBool();
        i->money = obj.value(Money).toDouble();

        items << i;
    }

    qDebug() << "qt: time: end foreach: " << (timer.elapsed() - prev) << "ms";
    prev = timer.elapsed();


    qDebug() << "qt: time: TOTAL obj elapsed:" << timer.elapsed() << "ms count:" << items.count();

    qDeleteAll(items);

    return prev;
}

qint64 Model::doLoadArr(int i)
{
    QGoToC *g = QGoToC::instance();

    QList<Item*> items;

    QElapsedTimer timer;

    qDebug() << "qt: time: arr start" << i;
    timer.start();
    qint64 prev = 0;

    QByteArray ba = g->goCall(i);

    qDebug() << "qt: time: end goCall" << i << ": " << (timer.elapsed() - prev) << "ms" << ", ba.size:" << ba.size();
    prev = timer.elapsed();

    QJsonDocument json = QJsonDocument::fromJson(ba);
    QJsonArray arr = json.array();

    qDebug() << "qt: time: end prepare json: " << (timer.elapsed() - prev) << "ms" << ", arr count:" << arr.size();
    prev = timer.elapsed();
    foreach (const QJsonValue &val, arr) {

        QJsonArray vr = val.toArray();

        Item *i = new Item();
        i->id = vr.at(0).toInt();
        i->name = vr.at(1).toString();
        //i->birthDay = QDateTime::fromMSecsSinceEpoch(vr.at(2).toInt());
        i->phone = vr.at(3).toString();;
        i->siblings = vr.at(4).toInt();
        i->spouse = vr.at(5).toBool();
        i->money = vr.at(6).toDouble();

        items << i;
    }

    qDebug() << "qt: time: end foreach: " << (timer.elapsed() - prev) << "ms";
    prev = timer.elapsed();

    qDebug() << "qt: time: TOTAL arr elapsed:" << timer.elapsed() << "ms count:" << items.count();

    qDeleteAll(items);

    return prev;
}

qint64 Model::loadC()
{
    Service s;

    QList<Item*> items;

    QElapsedTimer timer;

    timer.start();

    //    s.items();
    QList<Service::Item> sitems = s.items();
    foreach (const Service::Item &it, sitems) {

        Item *i = new Item();
        i->id = it.id;
        i->name = it.name;
        i->birthDay = it.birthDay;
        i->phone = it.phone;
        i->siblings = it.siblings;
        i->spouse = it.spouse;
        i->money = it.money;

        items << i;
    }

    qint64 prev = timer.elapsed();
    qDebug() << Q_FUNC_INFO << "elapsed:" << timer.elapsed() << "ms count:" << items.count();


    qDeleteAll(items);

    return prev;
}
