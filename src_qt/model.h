#ifndef MODEL_H
#define MODEL_H

#include <QString>
#include <QDateTime>

class Model
{
public:
    Model();

    struct Item {
        quint64 id;
        QString name;
        QDateTime birthDay;
        QString phone;
        int siblings;
        bool spouse;
        double money;
        Item() : id(0), siblings(0),
            spouse(false), money(0.){}
    };

    qint64 loadJSON();
    qint64 loadEasyJSON();
    qint64 loadArrRawJSON();

    qint64 loadC();

    qint64 doLoadObj(int i);
    qint64 doLoadArr(int i);

};

#endif // MODEL_H
