#include "service.h"

Service::Service()
{

}

QList<Service::Item> Service::items() const
{
    QList<Service::Item> list;
    QDateTime now = QDateTime::currentDateTime();
    for (int i = 0; i < 100000; ++i) {
        Item it;
        it.id = i;
        it.name = QString("Item %1").arg(i);
        it.birthDay = now;
        it.phone = "+79003517159";
        it.money = 42.01;

        list << it;
    }

    return list;
}
