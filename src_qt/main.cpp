#include <QCoreApplication>

#include <QElapsedTimer>
#include <QMap>
#include <QDebug>

#include "qgotoc.h"
#include "model.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QGoToC *b = QGoToC::instance();

    b->echo();

    b->debug(true);

    b->echoGo();
    //b->goCall(110);


    b->debug(false);

    b->benchGoAll();

    QMap<QString, QList<qint64> > stat;

    for (int i = 0; i < 3; ++i) {
        Model m;
        stat["1 loadJSON"] << m.loadJSON();
        stat["2 loadEasyJSON"] << m.loadEasyJSON();
        stat["3 loadArrRawJSON"] << m.loadArrRawJSON();
        stat["4 loadC"] << m.loadC();
    }

    foreach (const QString &key, stat.keys()) {

        quint64 sum = 0;
        const QList<qint64> list = stat[key];
        foreach (qint64 t, list) {
            sum += t;
        }

        qDebug() << "qt: time: AVG" << key << (sum/list.count()) << "ms";
    }


    return a.exec();
}
