#ifndef SERVICE_H
#define SERVICE_H

#include <QString>
#include <QList>
#include <QDateTime>

class Service
{
public:
    Service();

    struct Item {
      quint64 id;
      QString name;
      QDateTime birthDay;
      QString phone;
      int siblings;
      bool spouse;
      double money;
      Item() : id(0), siblings(0),
          spouse(false), money(0.) {}
    };

    QList<Item> items() const;
};

#endif // SERVICE_H
